import hickle as hkl
import subprocess
import os
import readline

#reads the defaults file for initial values
if os.path.isfile("./defaults.py"):
	print("using defaults.py config\n")
	tts_binary, standalonemodelname, standalonemodelpath, standalonemodelconfig, vocodermodelname, vocodermodelpath, vocoderconfigpath, encodermodelname, encodermodelpath, encoderconfigpath, text, referencewav, speakerwav, outputpath, languageidx = hkl.load("defaults.py")

elif not os.path.isfile("./defaults.py"):
	print("warning! config not found.")
	print("creating config with empty values\n")
	tts_binary, standalonemodelname, standalonemodelpath, standalonemodelconfig, vocodermodelname, vocodermodelpath, vocoderconfigpath, encodermodelname, encodermodelpath, encoderconfigpath, text, referencewav, speakerwav, outputpath, languageidx = "               "

#from https://stackoverflow.com/a/2533142
#uses readline for actual text editing
def rlinput(prompt, prefill=''):
   readline.set_startup_hook(lambda: readline.insert_text(prefill))
   try:
      return input(prompt)
   finally:
      readline.set_startup_hook()

#function for picking values for var between cfg and new and checks if they are suspect
def varchk(varv, whatis):
	while 1 == 1:
		print (whatis + " = " + varv)
		correct = rlinput("type new value, or press enter to confirm : ", varv)
		if correct == "":
			print ("confirmation accepted\n")
			return (varv)
			break
		else:
			varv = (correct)
			print ("edited value accepted\n")
			return (varv)
			continue
#enables options only if they have a value
def mtchk(arg, varv):
	if varv == "" or varv == " ":
		return ""
	else:
		brac = '"'
		return (arg + brac + varv + brac )
			
		  
#var checking
tts_binary            = varchk( tts_binary,"Coqui TTS Location")
standalonemodelname   = varchk( standalonemodelname, "Standalone Model Name")
standalonemodelpath   = varchk( standalonemodelpath,"Standalone Model Path")
standalonemodelconfig = varchk( standalonemodelconfig,"Standalone Model Config Path")
vocodermodelname      = varchk( vocodermodelname,"Vocoder Model Name")
vocodermodelpath      = varchk( vocodermodelpath,"Vocoder Model Path")
vocoderconfigpath     = varchk( vocoderconfigpath,"Vocoder Config Path")
encodermodelname      = varchk( encodermodelname, "Encoder Model Name")
encodermodelpath      = varchk( encodermodelpath, "Encoder Model Path")
encoderconfigpath     = varchk( encoderconfigpath, "Encoder Config Path")
text                  = varchk( text, "Text To Be Read")
referencewav          = varchk( referencewav, "Reference Audio(defines what is spoken)")
speakerwav            = varchk( speakerwav, "Speaker Audio(defines voice of speaker)")
outputpath            = varchk( outputpath, "Output Path(for audio)")
languageidx           = varchk( languageidx,"Language Id")

#combining args and vars
ftts_binary            = mtchk(""                        , tts_binary)
fstandalonemodelname   = mtchk(" --model_name="          , standalonemodelname)
fstandalonemodelpath   = mtchk(" --model_path="          , standalonemodelpath)
fstandalonemodelconfig = mtchk(" --model_config="        , standalonemodelconfig)
fvocodermodelname      = mtchk(" --vocoder_name="        , vocodermodelname)
fvocodermodelpath      = mtchk(" --vocoder_path="        , vocodermodelpath)
fvocoderconfigpath     = mtchk(" --vocoder_config_path=" , vocoderconfigpath)
fencodermodelname      = mtchk(" --encoder_name="        , encodermodelname)
fencodermodelpath      = mtchk(" --encoder_path="        , encodermodelpath)
fencoderconfigpath     = mtchk(" --encoder_config_path=" , encoderconfigpath)
ftext                  = mtchk(" --text="                , text)
freferencewav          = mtchk(" --reference_wav="       , referencewav)
fspeakerwav            = mtchk(" --speaker_wav="         , speakerwav)
foutputpath            = mtchk(" --out_path="            , outputpath)
flanguageidx           = mtchk(" --language_idx="        , languageidx)

#fuses together all finished vars into req command
ttscommand = (ftts_binary + fstandalonemodelname + fstandalonemodelpath + fstandalonemodelconfig + fvocodermodelname + fvocodermodelpath + fvocoderconfigpath + fencodermodelname + fencodermodelpath + fencoderconfigpath + ftext + fspeakerwav + freferencewav + foutputpath + flanguageidx)
print ("Command: " + ttscommand)
#runs da heckin coqui tts main binary
subprocess.call(ttscommand, shell=True)



#asks to save config
saveanswer = input("save config? [y]es [n]o (default:[y]es) : ")
if saveanswer == "y" or saveanswer == "Y" or saveanswer == "":
	print ("saving preferences..")
	hkl.dump([tts_binary, standalonemodelname, standalonemodelpath, standalonemodelconfig, vocodermodelname, vocodermodelpath, vocoderconfigpath, encodermodelname, encodermodelpath, encoderconfigpath, text, referencewav, speakerwav, outputpath, languageidx], "defaults.py")
	#TODO: add parameters for
	#<
	#targetdir \n 
	#targetwav \n 
	#sourcewav \n 
	#savespectrogram
	#>
elif saveanswer == "n" or saveanswer == "N":
	print ("not saving config..\n")
	exit

playanswer = input("play sample? [y]es [n]o (default:no)  : ")
if playanswer == "y" or playanswer == "Y":
	 subprocess.call(("/usr/bin/xdg-open " + outputpath), shell=True)
elif playanswer == "n" or playanswer == "N" or playanswer == "":
	print ("exiting...")
