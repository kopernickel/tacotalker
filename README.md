
## what is it?
A python script with minimal dependencies that functions as a wrapper for an existing Coqui-TTS install.

## how does it work?
1. runs a loop that shows you the default values for all options.
2. if you hit enter without typing anything, it takes the default/shown value as okay and moves on to the next option.
3. if you type the new value and hit enter, it'll display the new value, and ask you if it's correct.
4. you can enter a different value again(if shown value is still incorrect), or press enter and move on.
5. when all values are checked, it runs the command to launch Coqui-TTS with the equivalent options.
6. asks if you want to save the current options. options are y for yes, and n for no, or just hit enter for the default option.
7. offers to play the newly created audio, using the same options as the previous one.

## how do i install?
install 'subprocess', and 'hickle' with pipx.
```
pipx install subprocess hickle
```
if you don't have pipx, install it with
```
pip install pipx
```
then clone this repository with git
```
git clone https://codeberg.org/kopernickel/tacotalker
```
now just run it with python.
```
python <location of the folder>/tacotalker/tacotalker-tui.py
```
## what options are currently available?
General Options:

- Coqui TTS Location

- Text To Be Read[--text]

- Reference Audio(defines what is spoken)[--reference_wav]

- Speaker Audio(defines voice of speaker)[--speaker_wav]

- Output Path(for audio)[--out_path]

- Language Id[--language_idx]

Model Specific Options:

- Standalone Model:

- - Standalone Model Name[--model_name]

- - Standalone Model Path[--model_path]

- - Standalone Model Config[--model_config_path]

- Vocoder Model:

- - Vocoder Model Name[--vocoder_name]

- - Vocoder Model Path[--vocoder_path]

- - Vocoder Model Config[--vocoder_config]

- Encoder Model:

- - Encoder Model Name[--encoder_name]

- - Encoder Model Path[--encoder_path]

- - Encoder Model Config[--encoder_config_path]

## what still needs to be done?
- :x: make gui

- :x: add gif tutorial

- :x: add several more options

## how is it licensed?

"AGPL-3.0-or-later" is the license type for everything in this repository except for the files in the /Images/ folder which are instead licensed under "CC-BY-SA"